<?php
/**
 * Structur:
 * 1. Check E-Mail auf Richtigkeit | check_email()
 * 1a. check Mobile auf Richtigkeit | check_mobile()
 * 2. Check E-Mail auf Existenz in der DB User
 * 3. Save Anmeldungen in die DB Events
 * 4. Update User-uid in der DB Events
 * 5. Update Anzahl der Events in der DB User
 * 6. Check Mobile auf Existenz in der DB-mobile
 */
class MSNewsletterEventsDataImportAjax
{
    public $mobile;
    public $email;
    public $msg;
    public $emailUser;
    public $mobileUser;
    public $mobileUserNetz;
    public $club;
    public $db_events = "wiml_users_events";
    public $db_users  = "wiml_users";
    public $db_mobile = "wiml_mobile";

    public function __construct()
    {
      if ( !class_exists("MSMobileCheckActions") ){
        $this->msg = 'Class "MSMobileCheckActions" existiert nicht. Bitte aktiviere Plugin "ms_mbile". Class "MSMobileCheckActions" befindet sich in "mobile_check_actions.php"';
        return $msg;
      }

      $this->email  = isset($_REQUEST["email_address"])  ? $_REQUEST["email_address"]  : NULL;
      $this->mobile = isset( $_REQUEST["mobile"]) ? $_REQUEST["mobile"] : NULL;
      $this->club   = isset($_REQUEST["club"])  ? $_REQUEST["club"] : NULL;

      if ( !$this->email ){
        return ( 'Keine E-Mailadresse' );
      }
      if ( !$this->mobile ){
        return 'Keine Mobile-Nummer';
      }
      if ( !$this->club ){
        return 'Kein Club';
      }
    }
    /**
   *  fn
   */
    public static function delete()
    {
      global $wpdb;
      $event_id     = 0;
      $event_datum  = '';
      extract( $_REQUEST );

      $MSNewsletterEventsDataImportAjax = new MSNewsletterEventsDataImportAjax;
      $table          = $MSNewsletterEventsDataImportAjax->db_events;
      $where          = array("event_id" => $event_id, "event_datum" => $event_datum);
      $where_format   = array( '%d', '%s');
      $result         = $wpdb->delete( $table, $where, $where_format = null );

      $anz = $wpdb->get_var("SELECT COUNT(*) FROM " . $table . " WHERE event_id ='".$event_id."' AND event_datum='". $event_datum ."'");
      wp_send_json( array("statustext" => $anz) );
      wp_die();
    }
    /**
    *  fn
    */
    public static function insert()
    {
       $MSNewsletterEventsDataImportAjax = new MSNewsletterEventsDataImportAjax;
       $mobile = $MSNewsletterEventsDataImportAjax->check_mobile(); /* Check Mobile | MobileNummer */
       $emailMSG = $MSNewsletterEventsDataImportAjax->check_email(); /* array("status" => true, "email" => $email, "statustext" => $msg[0]) */

       if( @$emailMSG["status"] == true){

         $saveEventsDBEvents  = $MSNewsletterEventsDataImportAjax->insertEvents();
         $msgEmailUser        = $MSNewsletterEventsDataImportAjax->emailUser(); /* UserData */
         $AnzEventsInUser     = $MSNewsletterEventsDataImportAjax->insertAnzEventsInUser(); /* FALSE | TRUE */
         $msgMobileUser       = $MSNewsletterEventsDataImportAjax->mobileUser(); /* UserData */

         wp_send_json(array(
           "statustextEventsAnz"    => $AnzEventsInUser,
           "statustextEmail"        => $MSNewsletterEventsDataImportAjax->email,
           "statustextEmailNewUser" => $msgEmailUser,
           "statustextMobile"       => $MSNewsletterEventsDataImportAjax->mobile,
           "statustextNewMobile"    => $msgMobileUser
         ));
       } else {
         wp_send_json(array(
           "statustextEventsAnz"    => '-',
           "statustextEmail"        => $MSNewsletterEventsDataImportAjax->email,
           "statustextEmailNewUser" => @$emailMSG["statustext"],
           "statustextMobile"       => '-',
           "statustextNewMobile"    => '-'
         ));
       }
       wp_die();
    }
    /**
    *  fn
    */
    public function emailUser()
    {
      global $wpdb;
      $sql_checkMail  = "SELECT * FROM ".$this->db_users." WHERE email_address='".$this->email."'";
      $row  = $wpdb->get_row($sql_checkMail);
      if ( is_object($row)  ){
        $this->emailUser = $row;
        $wpdb->query("UPDATE ".$this->db_events." SET uid='".$row->uid."' WHERE email_adress='".$this->email."'");
        return $this->email.' existiert bereits.';
      }

      $newEmailUserID   = $this->insertNewUserEmail();
      if ($newEmailUserID > 0 ){
        $sql              = "SELECT * FROM ".$this->db_users." WHERE uid='".$newEmailUserID."'";
        $newEmailUser     = $wpdb->get_row($sql);
        $this->emailUser  = is_object($newEmailUser) ? $newEmailUser :  NULL;

        return $newEmailUser->email_address. ' ist hinzugefügt.';
      } else {
        return 'Nicht importiert.';
      }
    }
    /**
      *  fn
    */
    public function insertNewUserEmail(){
      global $wpdb;

      $booking_confirm  = '';
      $firstname        = '';
      $lastname         = '';
      $firstname        = '';
      $lastname         = '';
      $event_name       = '';
      $booking_datum    = '';
      $club             = '';

      extract( $_REQUEST );
      $table = $this->db_users;
      $data = array(
        "confirm"			    => $booking_confirm,
        "email_address"	  => $this->email,
        "firstname"		    => $firstname,
        "lastname"		    => $lastname,
        "fullname"		    => $firstname .' '. $lastname,
        "herkunft"		    => $event_name,
        "herkunftsdatum"	=> $booking_datum,
        "club"			      => $club,
        "importdatum"		  => current_time("Y-m-d H:i:s"),
        "updatetime"		  => current_time("Y-m-d H:i:s")
      );
      $format = array('%s');
      $result = $wpdb->insert( $table, $data, $format );
      if( $result ){
        return $wpdb->insert_id;
      }
      return 0;
    }
    /**
    *  fn
    */
    public function insertAnzEventsInUser(){
      global $wpdb;
      $anz = $wpdb->get_var( "SELECT COUNT(*) FROM ".$this->db_events." WHERE email_adress='".$this->email."'");
      $wpdb->query("UPDATE ".$this->db_users." SET event_anz='".$anz."' WHERE email_address='".$this->email."'");
      return $anz;
    }
    /**
    *  fn
    */
   public function insertEvents(){
     global $wpdb;
     $event_id         = '';
     $event_name       = '';
     $event_datum      = '';
     $booking_datum    = '';
     $firstname        = '';
     $lastname         = '';
     $booking_web      = '';
     $booking_confirm  = '';

     extract( $_REQUEST );
     $table = $this->db_events;

     $data = array(
       "event_id"        => $event_id,
       "event_name"      => $event_name,
       "event_datum"     => $event_datum,
       "booking_datum"   => $booking_datum,
       "firstname"       => $firstname,
       "lastname"        => $lastname,
       "email_adress"    => $this->email,
       "mobile"          => $this->mobile,
       "booking_web"     => $booking_web,
       "booking_confirm" => $booking_confirm,
     );
     $format = array('%s');
     $result = $wpdb->insert( $table, $data, $format );
     return $result;
  }
  /**
  *  fn
  */
  public function mobileUser()
  {
      global $wpdb;
      if( !$this->mobileUserNetz ){
        return 'Kein Netz';
      }
      if( $this->mobile == false ){
        return $this->mobile . ' ist ungültig.';
      }
      if( !is_object($this->emailUser) ){
        return '$this->emailUser ist ungültig.';
      }

      $row = $wpdb->get_row("SELECT * FROM ".$this->db_mobile." WHERE mobile='".$this->mobile."'");
      if ( is_object($row) ){
        $this->mobileUser = $row;
        return $this->mobile . ' exitiert bereits.';
      }

      /* Exists UID */
      if( $this->emailUser->uid && $this->mobile != false ){
          $this->insertMobileInDBMobile();
          return $this->mobile . ' ist hinzugefügt.';
      }

      return $this->mobile . ' hat keinen User.';
  }
  /**
  *  fn
  */
  public function insertMobileInDBMobile(){
    global $wpdb;
    $firstname      = '';
    $lastname       = '';
    $event_name     = '';
    $booking_datum  = '';
    $club           = '';

    extract( $_REQUEST );

    $sql = "INSERT ".$this->db_mobile." SET
				uid				      ='".$this->emailUser->uid."',
				mobile          = '".$this->mobile."',
				name			      = '".$firstname .' '. $lastname."',
				herkunft		    = '".apply_filters('the_title', $event_name)."',
				herkunftsdatum	= '".$booking_datum."',
				club			      = '".$club."',
				netz			      = '".$this->mobileUserNetz."',
        importdatum     = now(),
				updatetime		  = now()";

		$wpdb->query( $sql );
  }
  /**
  *  fn
  */
  public function check_email(){

    $email = strtolower($this->email);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      return array("status" => false, "statustext" => "Filter invalid");
    }
    if( !$this->check_email_domain( $email ) ){
      return array("status" => false, "statustext" => "MX invalid");
    }
    $msg = $this->verifyEmail($email, 'gaesteliste@meinnachtleben.de', true);
    if( $msg[0] == "invalid" ){
      return array("status" => false, "email" => $email, "statustext" => $msg[0] .'<br>'. $msg[1]);
    }
    /* Main */
    $this->email = $email;
    return array("status" => true, "email" => $email, "statustext" => $msg[0]);
  }
   /**
    *  fn
    */
   public function check_email_domain( $email, $record = 'MX' ){
     list($user, $domain) = explode('@', $email);
     return checkdnsrr($domain, $record);
   }
   /**
    *  fn
    */
   public function verifyEmail($toemail, $fromemail, $getdetails = false){
     	$email_arr  = explode("@", $toemail);
      $details    = '';
      $mx_ip      = TRUE;
     	$domain     = array_slice($email_arr, -1);
     	$domain     = $domain[0];
      $result     = 'invalid';
     	// Trim [ and ] from beginning and end of domain string, respectively
     	$domain     = ltrim($domain, "[");
     	$domain     = rtrim($domain, "]");
     // 	if( "IPv6:" == substr($domain, 0, strlen("IPv6:")) ) {
     // 		$domain = substr($domain, strlen("IPv6") + 1);
     // 	}
     // 	$mxhosts = array();
     // 	if( filter_var($domain, FILTER_VALIDATE_IP) )
     // 		$mx_ip = $domain;
     // 	else
     // 		getmxrr($domain, $mxhosts, $mxweight);
     // 	if(!empty($mxhosts) )
     // 		$mx_ip = $mxhosts[array_search(min($mxweight), $mxhosts)];
     // 	else {
     // 		if( filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ) {
     // 			$record_a = dns_get_record($domain, DNS_A);
     // 		}
     // 		elseif( filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) ) {
     // 			$record_a = dns_get_record($domain, DNS_AAAA);
     // 		}
     // 		if( !empty($record_a) )
     // 			$mx_ip = $record_a[0]['ip'];
     // 		else {
     // 			$result   = "invalid";
     // 			$details .= "No suitable MX records found.";
     // 			return ( (true == $getdetails) ? array($result, $details) : $result );
     // 		}
     // 	}
      if ( $mx_ip ){
        $result = "valid";
      }
     	if($getdetails){
     		return array($result, $details);
     	}
     	else{
     		return $result;
     	}
   }

   /**
   *  fn
   */
  public function check_mobile(){
    if ( !class_exists("MSMobileCheckActions") ){
      return $this->msg;
    }

    $MSMobileCheckActions = new MSMobileCheckActions;
    $netz                 = $MSMobileCheckActions->netz();
    $this->mobileUserNetz = false;

    if ( $netz ){
      $this->mobileUserNetz = $netz;
      $this->mobile         = $MSMobileCheckActions->checkVorwahl();
      return $this->mobile;
    }
    return false;
  }

}
