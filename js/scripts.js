jQuery(document).ready(function($){
	 $("#html-view").click ( function(){

		var opened = tb_show('Vorschau', $(this).attr("href") + '?KeepThis=true&TB_iframe=true&height=900&width=500');

		setTimeout( function(){
			$("#TB_window, #TB_window iframe").css({"width" : 960});
		}, 500);
		return false;
	});

    setTimeout( function(){
       get_msl_urls();
    }, 3000);
    function get_msl_urls(){
        var out = '';
        $(".mce-edit-area iframe").contents().find("a").each( function( index ){
            //console.log( $(this).attr("href") );
            out += '<p>'+ index + ':  <input value="'+ $(this).attr("href") +'" style="width:90%"></p>';
        });
        out += '<p><a class="msl_urls_save button button-primary" href="#">Links Speichern</a></p>';
        $(".report_urls").html( out );
    }
    $("body").on( "click", ".msl_urls_save", function(){
        var area_contents = $(".mce-edit-area iframe").contents();
        $(".report_urls input").each( function( index ){
            //console.log( index + '/' + $(this).val() );
            area_contents.find("a:eq("+index+")").attr("href", $(this).val());
            area_contents.find("a:eq("+index+")").attr("data-mce-href", $(this).val());

        });
        setTimeout( function(){
           $("#post-query-submit").click();
        }, 1000);
        return false;
    });


	var postbox_mailing_club = "#postbox-mailinglist-groupby-club";
	var postbox_mailing_send = "#postbox-mailinglist-send";
	var postbox_mailinglist_sql_supermailer = "#postbox-mailinglist-sql-supermailer";
	$("body")
		.on( "click", postbox_mailing_club + ' tbody a', function(){
			$.ajax({
				url: $(this).attr("data-href"),
				dataType: 'json',
        type:'GET',
				beforeSend: function(formData, jqForm, options) {
					$(".notice").remove();
				},
				success : function(response) {
					$(postbox_mailing_send + ' .inside').load(  $(postbox_mailing_send + ' .inside').attr("data-href") );
					$(postbox_mailinglist_sql_supermailer + ' .inside').load(  $(postbox_mailinglist_sql_supermailer + ' .inside').attr("data-href") );
					$(postbox_mailinglist_sql_supermailer).prepend('<div class="notice notice-'+response.status+'"><p>'+response.statustext+'</p></div>');
				},
        error : function(exception){
          console.log(exception.responseText);
        }
			});
			return false;
		})
		.on( "click", postbox_mailing_send + ' tbody .delete a', function(){
			$.ajax({
				url: $(this).data("href"),
				data: "",
				dataType: 'json',
				type:'get',
				beforeSend: function(formData, jqForm, options) {
					$(".notice").remove();
				},
				success : function(response){

					$(postbox_mailing_send + ' .inside').load( $(postbox_mailing_send + ' .inside').data("href") );
					$(postbox_mailinglist_sql_supermailer + ' .inside').load( $(postbox_mailinglist_sql_supermailer + ' .inside').data("href") );
					$(postbox_mailinglist_sql_supermailer).prepend('<div class="notice notice-'+response.status+'"><p>'+response.statustext+'</p></div>');
				},
        error : function(exception){
          console.log(exception.responseText);
        }
			});
			return false;
		})
		.on( "click", "#MSNewsletterEventsDataImport", function(){
			var columnClass = '.result';
      var tableDataSelector = '#the-list tr';
      var count = 1;
      var gesamt = $( tableDataSelector ).length;
			var MSNewsletterEventsDataImport = $(this);
			var MSNewsletterEventsDataClubs = $("#MSNewsletterEventsDataClubs");

			if( MSNewsletterEventsDataClubs.val() === "" ){
				alert(	"Club auswählen!" );
				return '';
			}
			/* DELETE table*/
			$( "#MSNewsletterEventsDataDelete" ).click();
      /*Progressbar*/
      addProgressbar();
			$( tableDataSelector ).each( function (index ){
          var $this = $(this);

          setTimeout( function(){

						var parameter = '&event_name='+ $this.find(".event_name").text();
						parameter += '&event_datum='+ $this.find(".event_datum").text();
						parameter += '&booking_id='+ $this.find(".booking_id").text();
						parameter += '&booking_datum='+ $this.find(".booking_date").text();
						parameter += '&firstname='+ $this.find(".firstname").text();
						parameter += '&lastname='+ $this.find(".lastname").text();
						parameter += '&email_address='+ $this.find(".email_address").text();
						parameter += '&mobile='+ $this.find(".mobile").text();
						parameter += '&booking_web='+ $this.find(".booking_web").text();
						parameter += '&booking_confirm='+ $this.find(".booking_confirm").text();
						parameter += '&club='+ MSNewsletterEventsDataClubs.val();
						// alert( MSNewsletterEventsDataImport.data("href") + parameter );
						// console.log( MSNewsletterEventsDataImport.data("href") + parameter );
            $.ajax({
      				url: MSNewsletterEventsDataImport.data("href") + parameter,
      				data: "",
      				dataType: 'json',
      				type:'get',
              success : function(response) {

                $this.find( columnClass ).html( response.statustextNewMobile );
								$this.find( '.email_result' ).html( 'Events-Anzahl:'+ response.statustextEventsAnz +'<br>'+ response.statustextEmailNewUser);

								var anz = count;
								var percentage = Math.ceil(anz / gesamt * 100);
		            var args = {};
		            args.count = anz;
		            args.gesamt = gesamt;
		            args.percentage = percentage;
                // console.log( args );
		            animateProgressbar(args);
								if ( response.statustextEmailNewUser.indexOf("hinzugefügt") >= 0 ){
		            	$this.addClass("selected");
								}
								$("#MSNewsletterEventsDataDeleteValue").html( anz );
								count++;
      				},
              error : function(exception){
                console.log( exception.responseText );
              }
      			});


          }, index*100);
      });
			return false;
		})
		.on( "click", "#MSNewsletterEventsDataDelete", function(){
			var $this = $(this);
			var count = 1;
      var gesamt = 1;
			addProgressbar();
			// console.log( $this.data("href") );
			$.ajax({
				url: $this.data("href"),
				data: "",
				dataType: 'json',
				type:'get',
				success : function(response) {

					var percentage = Math.ceil(count / gesamt * 100);
					var args = {};
					args.count = count;
					args.gesamt = gesamt;
					args.percentage = percentage;
					animateProgressbar(args);

					$("#MSNewsletterEventsDataDeleteValue").html( response.statustext );
				},
        error : function( exception ){
          console.log( exception.responseText );
        }
			});
		})
		.on("click", "#newsletterUnsubscribeButton", function(){
			var count = 1;
			var tableDataSelector = $("table tr");
      var gesamt = tableDataSelector.length;
			addProgressbar();

			tableDataSelector.each( function (index ){
          var $this = $(this);
          setTimeout( function(){
						$.ajax({
      				url: $this.find(".result").data("href"),
      				data: "",
      				dataType: 'json',
      				type:'get',
              success : function(response) {

                $this.find( ".result" ).html( response.statustext );

								var anz = count;
								var percentage = Math.ceil(anz / gesamt * 100);
		            var args = {};
		            args.count = anz;
		            args.gesamt = gesamt;
		            args.percentage = percentage;
		            animateProgressbar(args);

		            $this.addClass("selected");
								count++;
      				}
      			});
					}, index*100);
			});
			return false;
		});
	if( $("#newsletter-stats-userlist").length > 0 ){
		var newsletter_stats_userlist = $("#newsletter-stats-userlist");
		newsletter_stats_userlist.find("tr").each(function(){
			var firstname= $(this).find(".firstname").text().toLowerCase(),
					lastname = $(this).find(".lastname").text().toLowerCase(),
					email_address=$(this).find(".email_address").text().toLowerCase();

					$(this).addClass("error");
					if (email_address.indexOf(firstname) >= 0 || email_address.indexOf(lastname) >= 0){
						$(this).removeClass("error");
					}
		});
	}
  if ( $(".importUserAnz").length > 0 ){
    var importUserAnz = $(".importUserAnz");
    var event_id  = 0;
    var href       = 0;
    importUserAnz.each( function(){
      href        = $(this).find("span").attr("data-href");
      var $this   = $(this);
      // console.log( href );
      $.getJSON( href, function( data ){
          $this.find("span").html( data.anz );
      });

    });
  }
});
